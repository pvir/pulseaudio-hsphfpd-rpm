pulseaudio-hsphfpd-rpm
======================

Builds RPM with patched pulseaudio + hsphfpd daemon, from the sources at

* https://gitlab.freedesktop.org/pulseaudio/pulseaudio/-/merge_requests/288
* https://gitlab.freedesktop.org/pali/pulseaudio.git
* https://github.com/pali/hsphfpd-prototype
* https://github.com/pali/libopenaptx

The RPM can be installed alongside the usual Fedora pulseaudio
packages with no conflicts.

Tested only on Fedora 33, and not planning to support other OS.  This
is mostly a hack (and does not follow all Fedora packaging
guidelines), intended as a temporary workaround until the features are
merged in pulseaudio.

To build:

    sudo dnf build-dep pulseaudio
    sudo dnf install rpm-build rpm-plugin-selinux selinux-policy-devel glibc-static pulseaudio
    ./build.sh

It will produce RPM file at:

    $HOME/rpmbuild/RPMS/x86_64/pulseaudio-hsphfpd-14.0-*.fc33.x86_64.rpm

which you can `sudo dnf install`.

To activate after installation, you need to either log out,
or do

    systemctl --user daemon-reload
    systemctl --user restart pulseaudio.service

Additionally, you may need to remove `~/.config/pulse` before
logout/systemctl restart (this should be safe – pulseaudio recreates
it if necessary – but may result to forgetting current pulseaudio
settings).

The package includes patched pulseaudio fully installed under prefix
`/usr/lib/pulseaudio-hsphfpd` and an associated Systemd service
override file that enables it for user sessions.
Additionally, the package contains the hsphfpd service, and a Systemd
service file and SELinux module for it. The SELinux module *does not*
sandbox the service, but instead punches a specific hole in the Fedora
SELinux profile that is necessary for the daemon to function.  The
daemon runs with basically unrestricted root permissions.
