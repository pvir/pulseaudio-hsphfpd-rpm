#!/bin/bash

set -e -E -o pipefail

err_report() {
  echo "error: at line $(caller)" >&2
}

trap err_report ERR


cd "$(dirname "$0")"

pulseaudio_repo="https://gitlab.freedesktop.org/pali/pulseaudio.git"
hsphfpd_repo="https://github.com/pali/hsphfpd-prototype"
libopenaptx_repo="https://github.com/pali/libopenaptx"

version_tag="14.0"
pulseaudio_src="pulseaudio-hsphfpd-${version_tag}.tar.xz"

pulseaudio_branch=origin/hsphfpd
hsphfpd_branch=origin/prototype
libopenaptx_branch=0.2.0

if [ ! -d pulseaudio ]; then
    git clone "${pulseaudio_repo}" pulseaudio
fi
git -C pulseaudio fetch origin

if [ ! -d hsphfpd ]; then
    git clone "${hsphfpd_repo}" hsphfpd
fi
git -C hsphfpd fetch origin

if [ ! -d libopenaptx ]; then
    git clone "${libopenaptx_repo}" libopenaptx
fi
git -C libopenaptx fetch origin

date=`date -u +%Y%m%d`
pulseaudio_commit=`git -C pulseaudio rev-parse "${pulseaudio_branch}"`
hsphfpd_commit=`git -C hsphfpd rev-parse "${hsphfpd_branch}"`
libopenaptx_commit=`git -C libopenaptx rev-parse "${libopenaptx_branch}"`

sed -e "s/@PA_MAJOR@/${version_tag}/g; s/@DATE@/${date}/g; s/@PULSEAUDIO_COMMIT@/${pulseaudio_commit}/g; s/@HSPHFPD_COMMIT@/${hsphfpd_commit}/g; s/@LIBOPENAPTX_COMMIT@/${libopenaptx_commit}/g;" < pulseaudio-hsphfpd.spec.in > pulseaudio-hsphfpd.spec

git -C pulseaudio archive --format=tar --prefix="pulseaudio-hsphfpd-${version_tag}/" "${pulseaudio_commit}" > "pulseaudio.tar"
git -C hsphfpd archive --format=tar --prefix="pulseaudio-hsphfpd-${version_tag}/hsphfpd/" "${hsphfpd_commit}" > "hsphfpd.tar"
git -C libopenaptx archive --format=tar --prefix="pulseaudio-hsphfpd-${version_tag}/libopenaptx/" "${libopenaptx_commit}" > "libopenaptx.tar"

rm -rf "pulseaudio-hsphfpd-${version_tag}"
rm -f "pulseaudio-hsphfpd-${version_tag}.tar" "pulseaudio-hsphfpd-${version_tag}.tar.xz"
mkdir "pulseaudio-hsphfpd-${version_tag}"
cp -a selinux "pulseaudio-hsphfpd-${version_tag}/selinux"
cp -f hsphfpd.service "pulseaudio-hsphfpd-${version_tag}/hsphfpd.service"
tar Af "pulseaudio-hsphfpd-${version_tag}.tar" "pulseaudio.tar"
tar Af "pulseaudio-hsphfpd-${version_tag}.tar" "hsphfpd.tar"
tar Af "pulseaudio-hsphfpd-${version_tag}.tar" "libopenaptx.tar"
tar uf "pulseaudio-hsphfpd-${version_tag}.tar" "pulseaudio-hsphfpd-${version_tag}"
xz "pulseaudio-hsphfpd-${version_tag}.tar"
rm -f "pulseaudio.tar" "hsphfpd.tar" "libopenaptx.tar"
rm -rf "pulseaudio-hsphfpd-${version_tag}"

mkdir -p "${HOME}/rpmbuild/SOURCES"
ln -sf "${PWD}/${pulseaudio_src}" "${HOME}/rpmbuild/SOURCES/${pulseaudio_src}"

exec rpmbuild -bb pulseaudio-hsphfpd.spec
